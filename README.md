### Installation Of AlphaClient ###
1. Send Folder AlphaClient to /opt folder
```
cd /opt
git clone https://gitlab.com/fintex1/alphaclient/
cd alphaclient/
chmod +x install.sh
```
2. Copy .env.example to .env
3. Edit .env file end setting
```
MASTERIP=<ippublic_masterserver>
NAMENODE=<namenode>
```
example
```
MASTERIP=104.154.150.135
NAMENODE=node1
```
4. Run 
```./install.sh ```
follow instruction for install AlphaClient on Node Server
5. Node Automatically Sync With Master Server And Please make a distinction between nodes