import requests
from subprocess import Popen, PIPE
from time import sleep

# read file .env
file = open('/opt/alphaclient/.env', 'r').read().splitlines()
for i in file:
    i = i.split('=')
    if i[0] == 'MASTERIP':
        masterip = i[1]
    if i[0] == 'NAMENODE':
        node = i[1]

def shell(cmd):
    return Popen(cmd, shell=True, stdout=PIPE).stdout.read().decode('utf-8')

def sendstat():
    url = f"http://{masterip}/alphaserver/home/api/9875badc798b5ad78cb5978dac9a78cb97a9dcdba9"
    print(url)
    payload=f'name_node={node}'
    headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    print(response.text)

last = ''
while True:
    try:
        data = shell('cat /var/log/auth.log | tail -100 | grep "password for"').splitlines()[-1]
        if data != last:
            last = data
            sendstat()
    except Exception as e:
        print(e)
        continue
    sleep(1)
        



