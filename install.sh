#!/bin/bash
# Minimum requirements:
# - Ubuntu 18.04 LTS [Tested]

# check pwd on /opt/alphaclient
if [ "$(pwd)" != "/opt/alphaclient" ]; then
    echo "Folder Should be on /opt/alphaclient (Case Sensitive Implemented)"
    exit 1
fi

# check root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# check curl installed
if [ -f /usr/bin/curl ]; then
    echo "curl is installed"
else
    echo "curl is not installed"
    echo "Installing curl"
    sudo apt-get install curl -y
fi

#check python3 installed
if [ -f /usr/bin/python3 ]; then
    echo "python3 is installed"
else
    echo "python3 is not installed"
    echo "Installing python3"
    sudo apt-get install python3 python3-pip -y
    sudo python3 -m pip install --upgrade pip
    sudo python3 -m pip install --upgrade setuptools
    sudo python3 -m pip install requests
fi

# check env
if [ -f /opt/alphaclient/.env ]; then
    echo "environment file is installed"
else
    echo "environment file is not installed"
    echo "Please Copy .env.example to .env environment file"
    exit 1
fi


# install service
echo "Installing service"
sudo cp /opt/alphaclient/alphaclient.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable alphaclient.service
sudo systemctl start alphaclient.service











